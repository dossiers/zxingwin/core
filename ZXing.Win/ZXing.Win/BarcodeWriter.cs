﻿/*
 * Copyright 2012 ZXing.Net authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using Windows.UI.Xaml.Media.Imaging;
using ZXing.Common;
using ZXing.Rendering;
using ZXing.Writer;

namespace ZXing
{
#if UNITY
   /// <summary>
   /// A smart class to encode some content to a barcode image
   /// </summary>
   public class BarcodeWriter : BarcodeWriterGeneric<UnityEngine.Color32[]>, IBarcodeWriter
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="BarcodeWriter"/> class.
      /// </summary>
      public BarcodeWriter()
      {
         Renderer = new Color32Renderer();
      }
   }
#else

   /// <summary>
   /// A smart class to encode some content to a barcode image
   /// </summary>
   public class BarcodeWriter : BarcodeWriterGeneric<Windows.UI.Xaml.Media.Imaging.WriteableBitmap>, IBarcodeWriter
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="BarcodeWriter"/> class.
      /// </summary>
      public BarcodeWriter()
      {
            // TBD:
            // ?????
            // Renderer = new WriteableBitmapRenderer();
            // ....
      }

        // TBD:
        WriteableBitmap IBarcodeWriter.Write(BitMatrix matrix)
        {
            throw new NotImplementedException();
        }

        WriteableBitmap IBarcodeWriter.Write(string contents)
        {
            throw new NotImplementedException();
        }
    }
#endif

    /// <summary>
    /// A smart class to encode some content to a svg barcode image
    /// </summary>
    public class BarcodeWriterSvg : BarcodeWriterGeneric<SvgRenderer.SvgImage>
   {
      /// <summary>
      /// Initializes a new instance of the <see cref="BarcodeWriter"/> class.
      /// </summary>
      public BarcodeWriterSvg()
      {
            // TBD:
            // ????
            // Renderer = new SvgRenderer();
            // ....
      }
   }
}
