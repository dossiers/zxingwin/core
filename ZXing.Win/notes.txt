[ Miscellaneous notes ]

How to create a nuget package:
(http://docs.nuget.org/ndocs/create-packages/creating-a-package)

VS: -> Release: Rebuild Solution.
cd C:\...\core\ZXing.Win\ZXing.Win.Core
C:\programs\nuget\nuget.exe spec


cd ZXing.Win.Core
C:\programs\nuget\nuget.exe pack ZXing.Win.Core.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.OneD
C:\programs\nuget\nuget.exe pack ZXing.Win.OneD.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.TwoD
C:\programs\nuget\nuget.exe pack ZXing.Win.TwoD.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.QRCode
C:\programs\nuget\nuget.exe pack ZXing.Win.QRCode.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.Aztec
C:\programs\nuget\nuget.exe pack ZXing.Win.Aztec.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.DataMatrix
C:\programs\nuget\nuget.exe pack ZXing.Win.DataMatrix.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.MaxiCode
C:\programs\nuget\nuget.exe pack ZXing.Win.MaxiCode.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.Multi
C:\programs\nuget\nuget.exe pack ZXing.Win.Multi.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.PDF417
C:\programs\nuget\nuget.exe pack ZXing.Win.PDF417.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win.Client
C:\programs\nuget\nuget.exe pack ZXing.Win.Client.csproj -Prop Configuration=Release
cd ..

cd ZXing.Win
C:\programs\nuget\nuget.exe pack ZXing.Win.csproj -Prop Configuration=Release
cd ..






