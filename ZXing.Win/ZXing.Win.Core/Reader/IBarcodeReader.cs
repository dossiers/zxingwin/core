﻿/*
 * Copyright 2012 ZXing.Net authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using ZXing.Common;
using ZXing.Core;
//#if UNITY
//using UnityEngine;
//#else
//using Windows.UI.Xaml.Media.Imaging;
//#endif

namespace ZXing.Reader
{
   /// <summary>
   /// Interface for a smart class to decode the barcode inside a bitmap object
   /// </summary>
   public interface IBarcodeReader : IBarcodeDecoder
    {
#if UNITY
      /// <summary>
      /// Decodes the specified barcode bitmap.
      /// </summary>
      /// <param name="rawColor32">The image as Color32 array.</param>
      /// <returns>the result data or null</returns>
      Result Decode(UnityEngine.Color32[] rawColor32, int width, int height);
#else
        /// <summary>
        /// Decodes the specified barcode bitmap.
        /// </summary>
        /// <param name="barcodeBitmap">The barcode bitmap.</param>
        /// <returns>the result data or null</returns>
        Result Decode(Windows.UI.Xaml.Media.Imaging.WriteableBitmap barcodeBitmap);
#endif
   }
}
